import React from "react";
import agent from "../agent";
import DeleteButton from "./delete-button";

const AddressDeleteButton = ({ address, ...props }) => {
    return (
        <DeleteButton
            title="Hapus Alamat"
            message={`Apakah Anda yakin ingin menghapus alamat?`}
            action={() => agent.Address.destroy(address.id)}
            {...props}
        />
    );
};

export default AddressDeleteButton;
