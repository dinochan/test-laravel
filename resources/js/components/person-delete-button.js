import React from "react";
import agent from "../agent";
import DeleteButton from "./delete-button";

const PersonDeleteButton = ({ person, ...props }) => {
    return (
        <DeleteButton
            title="Hapus Kontak"
            message={`Apakah Anda yakin ingin menghapus ${person.name} ?`}
            action={() => agent.Person.destroy(person.id)}
            {...props}
        />
    );
};

export default PersonDeleteButton;
