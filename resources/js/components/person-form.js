import React from "react";
import Form from "../components/form";
import { useToast } from "@chakra-ui/react";

const fields = [
    { name: "name", label: "Nama Lengkap", type: "text", value: "" },
    { name: "birthday", label: "Tanggal Lahir", type: "date", value: "" },
    {
        name: "gender",
        label: "Jenis Kelamin",
        type: "radio",
        options: [
            { value: "0", label: "Wanita" },
            { value: "1", label: "Pria" },
        ],
        value: "",
    },
];

const PersonForm = ({
    action,
    onSuccess = () => {},
    person = {},
    ...props
}) => {
    const toast = useToast();

    const onSubmit = (data, { setSubmitting, setErrors }) => {
        setSubmitting(true);
        action(data)
            .then((person) => {
                toast({ title: "Berhasil Simpan!", status: "success" });
                onSuccess(person);
            })
            .catch(({ errors, message }) => {
                if (errors) {
                    setErrors(errors);
                }
            })
            .finally(() => setSubmitting(false));
    };

    return (
        <Form
            fields={fields}
            initialValues={person}
            onSubmit={onSubmit}
            {...props}
        />
    );
};

export default PersonForm;
