<?php

return [
    'required' => ':attribute harus diisi.',
    'integer' => ':attribute format data harus integer.',
    'date' => ':attribute format tanggal tidak sesuai',

    'attributes' => [
        'name' => 'Nama Lengkap',
        'birthday' => 'Tanggal Lahir',
        'gender' => 'Jenis Kelamin',
        'person_id' => 'Kontak',
        'address' => 'Alamat Lengkap',
        'post_code' => 'Kode Pos',
        'city_name' => 'Kota',
        'country_name' => 'Negara',
    ],
];
